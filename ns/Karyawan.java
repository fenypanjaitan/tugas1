package com.ns;

public class Karyawan {
    private int idCard;
    private String name;
    private int idParking;
    private String jabatan;
    private int salary;

//    public Karyawan(){}
    public Karyawan(int idCard, String name, int idParking, String jabatan, int salary){
        this.idCard = idCard;
        this.name = name;
        this.idParking = idParking;
        this.jabatan = jabatan;
        this.salary = salary;
    }

    public String toString() {
        return "id: "+this.idCard +"\nnama: " +this.name+"\nidParking: "+this.idParking
                +"\njabatan: "+this.jabatan+"\ngaji: "+this.salary*this.idCard +"\n "+"\n ";
    }

}

package com.ns;

import java.util.ArrayList;
import java.util.List;

public class App {
    private List<Karyawan> listKaryawan;

    public void setListKaryawan(List<Karyawan> list) {
        this.listKaryawan = new ArrayList<Karyawan>(list);
    }


    public static void main(String[] args) {
        App app = new App();

        List<Karyawan> list = new ArrayList<Karyawan>();
        list.add(new Karyawan(1,"karyawan1", 1,"karyawan",300));
        list.add(new Karyawan(2,"karyawan2", 2,"karyawan",300));
        list.add(new Karyawan(3,"karyawan3", 3,"karyawan",300));
        list.add(new Karyawan(4,"Manajer1", 4,"Manajer",1000));
        list.add(new Karyawan(5,"Manajer2", 5,"Manajer",1000));


        app.setListKaryawan(list);
        System.out.println(list);


    }
}

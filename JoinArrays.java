package com.ns;

import java.util.ArrayList;

public class JoinArrays {
    public static void main( String[] args ) {
        ArrayList<String> collection1 = new ArrayList<>();
        collection1.add("alpha");
        collection1.add("bravo");
        collection1.add("charlie");
        collection1.add("delta");
        collection1.add("echo");


        ArrayList<String> collection2 = new ArrayList<>();
        collection2.add("delta");
        collection2.add("echo");
        collection2.add("xray");
        collection2.add("yankee");

        System.out.println("============= List collection 1 ============= ");

        for (String name : collection1) {
            System.out.println(name);
        }

        System.out.println("\n============= List collection 2 ============= ");

        for (String name : collection2) {
            System.out.println(name);
        }

        System.out.println("\n============= Hasil ============= ");

        ArrayList<String> newCol = new ArrayList<>(collection1);
        newCol.retainAll(collection2);

        collection1.addAll(collection2);
        collection1.removeAll(newCol);

        for (String nato : collection1) {
            System.out.println(nato);
        }
    }
}
